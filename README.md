HOW TO INSTALL
==============

Create virtualenv and activate it

Install requirements  

	pip install -r requirements.txt

Change database settings accordingly

Create superuser  

	python manage.py createsuperuser  


Make migration  

	python manage.py migrate  


Generate ststic files and Start server  

	python manage.py collectstatic  
	python manage.py runserver  


Contact at +919599618855/dhirendra4work@gmail.com for any query