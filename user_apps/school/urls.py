# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import routers
from .api_views import SchoolViewSet
from .views import school_list,school_detail
from django.conf.urls import url

router = routers.SimpleRouter()
router.register(r'v1/schools', SchoolViewSet)
#urlpatterns = router.urls

urlpatterns = [
    url(r'^schools/$', school_list,name = 'school list'),
    url(r'^schools/(?P<school_id>[\d]+)/$', school_detail,name = 'school detail'),
]

urlpatterns = urlpatterns + router.urls