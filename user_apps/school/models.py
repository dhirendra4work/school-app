# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import gettext as _
from django.core.validators import MinValueValidator,MaxValueValidator

# Create your models here.
class School(models.Model):

	name = models.CharField(_('school name'),max_length=75)
	pincode = models.PositiveIntegerField(_('school pincode'),validators=[MinValueValidator(100000),MaxValueValidator(999999)])

	class Meta:
		unique_together = ('name','pincode')
