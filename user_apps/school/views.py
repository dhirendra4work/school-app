# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import Http404
from django.shortcuts import render
from .models import School

# Create your views here.
def school_list(request):
	school_list = School.objects.all()
	context = {"school_list":school_list}
	return render(request,'school/list.html',context)

def school_detail(request,school_id):
	try:
		school_obj = School.objects.get(id=school_id)
	except School.DoesNotExist:
		raise Http404("Question does not exist")
	context = {"school":school_obj}
	return render(request,'school/detail.html',context)
